DIY Outdoors
==================

**Theme Name:** DIY Outdoors

**Theme Description:** Theme for the Do It Yourself Outdoors site.

**Developers name(s):** Adam Johnson

**Dependencies necessary to work with this theme:** Sass. The sass files automatically generate CSS files via CleanSlate. No third party apps or CLI tools are needed.