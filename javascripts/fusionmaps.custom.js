//https://raw.github.com/sokai/jquery.getUrlParam/master/jquery.geturlparam.min.js
(function(a){a.fn.extend({getUrlParam:function(b){b=escape(unescape(b));var c=[],d=null,e=0;if(a(this).attr("nodeName")==="#document")escape(unescape(window.location.search)).search(b)>-1&&(d=window.location.search.substr(1,window.location.search.length).split("&"));else if(a(this).attr("src")!=="undefined"){var f=a(this).attr("src");if(f.indexOf("?")>-1){var g=f.substr(f.indexOf("?")+1);d=g.split("&")}}else{if(a(this).attr("href")==="undefined")return null;var f=a(this).attr("href");if(f.indexOf("?")>-1){var g=f.substr(f.indexOf("?")+1);d=g.split("&")}}if(d===null)return null;for(e;e<d.length;e++)escape(unescape(d[e].split("=")[0]))===b&&c.push(d[e].split("=")[1]);return c.length===0?null:c.length===1?c[0]:c}})})(jQuery)


      google.load('visualization', '1');
    
    markers = [];
    infoWindow = new google.maps.InfoWindow();
    tableID = '1AOWRAWNWEXNVj0DxA6rXCN6XdxH5jSXE-zL7Q1o';
    var directionsDisplay;
    var map;
    directionsDisplay = new google.maps.DirectionsRenderer();   
      directionsService = new google.maps.DirectionsService();
    printDisplay = new google.maps.DirectionsRenderer();  
    
    var filter = $(document).getUrlParam("filter");   
    //var loc = $(document).getUrlParam("loc");
    var loc = window.location.hash.slice(1);


    function setQuery(tableID,filter,loc){
      // Send query to Google Chart Tools to get data from table.
      // Note: the Chart Tools API returns up to 500 rows.
    var conditions;
    if (!loc){
      if (filter !== 'all' && filter !== ''){
        conditions = "WHERE type='"+filter+"'";
        $('.'+filter).addClass('active');
      } else { 
        conditions = '' 
      }
    } else {
      conditions = "WHERE name LIKE '%"+decodeURIComponent(loc)+"%'";
    }
    
    
      var statement = "SELECT * FROM "+tableID+' '+conditions;
    query = {}
      query.string = encodeURIComponent(statement);
    query.filter = filter;
      return query;
    }
    
    function doDirections(coords){
    directionsDisplay.setMap(map);
    // build directsion panel if it doesn't exist, otherwise clear it.
    if ($('#directionsPanel').length == 0){
      $('#map-canvas').append('<div id="directionsPanel"></div>');
      $('#directionsPanel').clone().attr('id','directionsClone').appendTo('#printDirections');
    } else {
      $('#directionsPanel').html('');
      $('#printDirections').html('');
    }
    directionsDisplay.setPanel(document.getElementById('directionsPanel'));
    printDisplay.setPanel(document.getElementById('directionsClone'));
                    
    var start = document.getElementById('begin').value;

    var request = {
      origin: start,
      destination: coords,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        clearMap(markers);
              
        directionsDisplay.setDirections(response);
        printDisplay.setDirections(response);
              
      } else {
        window.alert('Error generating directions: ' + status);
      }     
    });
      
    }
    
    
    var getLocationIcon = function(type){
      if (type == 'backpacking'){
        icon = '../images/icons/backpacking.png';
      } else if (type == 'hiking'){
        icon = '../images/icons/hiking.png';       
      } else if (type == 'camping'){
        icon = '../images/icons/tents.png';        
      } else if (type == 'bicycling'){
        icon = '../images/icons/cycling.png';        
      } else if (type == 'canoeing'){
        icon = '../images/icons/kayak1.png';       
      } else if (type == 'swimming'){
        icon = '../images/icons/swimming-2.png';       
      } else if (type == 'caving'){
        icon = '../images/icons/spelunking.png';       
      } else if (type == 'mountain-biking'){
        icon = '../images/icons/mountainbiking-3.png';       
      } else if (type == 'rock-climbing'){
        icon = '../images/icons/climbing.png';       
      } else if (type == 'whitewater'){
        icon = '../images/icons/kayaking.png';       
      } else if (type == 'downhill-skiing'){
        icon = '../images/icons/skiing.png';       
      } else if (type == 'xc-skiing'){
        icon = '../images/icons/nordicski.png';        
      } else {
        icon = '../images/icons/backpacking.jpg';        
      }
      return icon;
    }
      
    var createMarker = function(location,map) {
      //var str_coord = location.coordinate.split(',');
      var lng = location.longitude;
      var lat = location.latitude;
      var coords =  new google.maps.LatLng(lat, lng);
      var marker = new google.maps.Marker({
        map: map,
        position: coords,
        icon: getLocationIcon(location.type)
      }); 
  
      google.maps.event.addListener(marker, 'click', function(event) {
        infoWindow.setPosition(coords);
        infoWindow.setContent( 
          '<h3>' + location.name +'</h3>' +
          '<div class="description">'+ location.description + '</div>' +
          '<div class="url"><a href="' + location.url + '">' + location.url + '</a></div>' +
          '<div><span class="directions-title">Get Directions:</span><br />' +
          '<input type="text" placeholder="Enter Start Address" id="begin">' +
          '<input type="button" value="go" id="go">' +
          '</div>');
        infoWindow.open(map);

        google.maps.event.addDomListener(document.getElementById('go'), 'click', function() {
          doDirections(coords);
        });   
            
        google.maps.event.addDomListener(document.getElementById('begin'), 'keypress', function(e) {
          if (e.keyCode == 13){
            doDirections(coords);
          }
        });           
        
      });
      return ({marker: marker, info: infoWindow, name: location.name});
    };

            
    var buildMarkers = function(query,map,loc){
      var gvizQuery = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + query.string);
      gvizQuery.send(function(response) {
        var numRows = response.getDataTable().getNumberOfRows();
      
        //For each row in the table, create a marker
        for (var i = 0; i < numRows; i++) {
            
          var location = {};
          location.name = response.getDataTable().getValue(i, 0);
          location.type = response.getDataTable().getValue(i, 1);
          location.description = response.getDataTable().getValue(i, 2); 
          location.url = response.getDataTable().getValue(i, 3);
          location.image = response.getDataTable().getValue(i, 4);
          location.latitude = response.getDataTable().getValue(i,5);
          location.longitude = response.getDataTable().getValue(i,6);
      
          if (query.filter !== '' && query.filter !== 'all'){
            $('#places').append('<div data-id="'+i+'" data-coords="'+location.coordinate+'"><a href="#">'+location.name+'</a></div>');
          }     
                              
          marker = createMarker(location,map);
          markers.push(marker);
          
          if(loc !== false){
            centerpt = new google.maps.LatLng(location.latitude,location.longitude);
            map.panTo(centerpt);
            
            infoWindow.setPosition(centerpt);
            infoWindow.setContent( 
              '<h3>' + location.name +'</h3>' +
              '<div class="description">'+ location.description + '</div>' +
              '<div class="url"><a href="' + location.url + '">' + location.url + '</a></div>' +
              '<div><span class="directions-title">Get Directions:</span><br />' +
              '<input type="text" placeholder="Enter Start Address" id="begin">' +
              '<input type="button" value="go" id="go">' +
              '</div>');
            infoWindow.open(map);
            
          }

        }
      }); 
      

              
      }

    
      function initialize() {

        map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: new google.maps.LatLng(39.64396584874446,-79.9570369720459),
          zoom: 7,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    
        directionsDisplay.setMap(map);

    // Set up Query
    var conditions;
    if ( (filter == '') || (filter === null) || (filter === undefined) ) {
      conditions = 'all';
    } else {
      conditions = filter;
    }
    
      if ((loc == '') || (loc === undefined) || (loc === null)){
        loc = false;
      } 
    
    var query = setQuery(tableID,conditions,loc);
    buildMarkers(query,map,loc);
    
        
    //WV outlines example
    // layer = new google.maps.FusionTablesLayer({
    //  query: {
    //    select: '*',
    //    from: '1S-CINcNUKCSBgfY_G4TfVvfYbNV9T6BzCxF_9xo'
    //  },
    //  styles: [{
    //    polygonOptions: {
    //      fillColor: "#124aff",
    //      fillOpacity: 0.1
    //    }
    //  }, {
    //    where: "'County Name' = 'Monongalia'",
    //    polygonOptions: {
    //      fillColor: "#ffe612",
    //      fillOpacity: 0.1
    //    }
    //  }],
    //  map: map,
  //           options: {
  //             suppressInfoWindows: true
  //           }
    // });
      
    
    google.maps.event.addListener(map, 'zoom_changed', function() {
      zoomLevel = map.getZoom();
      if (zoomLevel <= 12) {
          layer.setMap(map);
      } else {
          layer.setMap(null);
      }
    });
    
    
    
    $('#filters li a').click(function(event){
      event.preventDefault();
      window.location.hash = '';
      clearDirections();
      filter = $(this).parent().attr('class');
      runFilter(filter,map);
      $('#filters li').removeClass('active');
      $(this).parent().addClass('active');
      
    });   
    
    }
  function clearDirections(){
    $('#directionsPanel').remove();
    directionsDisplay.setMap(null);
    
  }
  function clearMap(markers){
    $.each(markers,function(index,marker){
      marker.marker.setMap(null);
      marker.info.setMap(null);
    });
  }
  
  function runFilter(filter,map){
    clearMap(markers);
    markers = [];
    var query = setQuery(tableID,filter);
    $('#results, #places').html('');
    // map.setZoom(7); 
    loc = false;
    buildMarkers(query,map,loc);
  }

    google.maps.event.addDomListener(window, 'load', initialize);
  
  $('body').delegate('#places a','click',function(event){
    // event.preventDefault();
    // var coords = $(this).parent().data('coords').split(',');
    // latLng =  new google.maps.LatLng(parseFloat(coords[1]),parseFloat(coords[0]));
    // google.maps.event.trigger(markers[$(this).parent().data('id')].marker, 'click', {
    //   //latLng: new google.maps.LatLng(0, 0)
    // });
    // //markers[$(this).parent().data('id')].info.open(map);
    // map.setZoom(14); 
    // map.panTo(latLng);
  }); 